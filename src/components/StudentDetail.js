import React, { Component} from "react";
import StudentComparisonCheckbox from "./StudentComparisonCheckbox.js";

function RecordsTable(props) {
    let courses = Object.keys(props.recordList).sort();
    let rows = courses.map((subject, index) => {
        return (
            <tr key={index}>
                <td className="student-detail--table_name">{subject}</td>
                <td className="student-detail--table_score">{props.recordList[subject]}&#37;</td>
            </tr>
        );
    });

    return ( <table className="student-detail--table"><tbody>{rows}</tbody></table>);
}

class StudentDetail extends Component{
    constructor(props) {
        super(props);
        this.handleCompareClick = this.handleCompareClick.bind(this);
    }

    handleCompareClick(e) {
        e.stopPropagation();
        if (e.target.checked) {
            this.props.addStudentComparison(props.studentName);
        } else {
            this.props.removeStudentComparison(props.studentName);
        }
    }

    render() {
        return(
            <div className="view-panel">
                <h1>{this.props.activeStudent}: Course Scores</h1>
                <div className="student-detail">
                    <div className="student-detail--header">
                        <div className="student-detail--header_compare">Comparison List: <StudentComparisonCheckbox studentComparison={this.props.studentComparison} studentName={this.props.activeStudent} updateStudentComparison={this.props.updateStudentComparison} /></div>
                    </div>
                    <RecordsTable recordList={this.props.studentRecord} />
                </div>
            </div>
        );
    }
}

export default StudentDetail;