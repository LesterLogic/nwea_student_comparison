import React from "react";
import ReactDOM from "react-dom";
import { defaultState } from "./data/data.js";

import App from "./App.js";
import "./App.scss";

ReactDOM.render(
	<App defaultState={defaultState} />,
	document.getElementById("app")
);
