import React, { Component} from "react";
import StudentComparisonCheckbox from "./StudentComparisonCheckbox.js";

function StudentRecordRow(props) {
    function handleClick(e) {
        e.preventDefault();
        props.clickEvent(props.studentName);
    }

    return (
        <div className="course-roster--row" onClick={handleClick}>
            <div className="course-roster--row_student-name">{props.studentName}</div>
            <div className="course-roster--row_score">{props.studentScore}&#37;</div>
            <div className="course-roster--row_student-compare"><StudentComparisonCheckbox studentComparison={props.studentComparison} studentName={props.studentName} updateStudentComparison={props.updateStudentComparison} /></div>
        </div>
    );
};

function StudentTable(props) {
    let studentListSorted = Object.keys(props.studentList).sort();
    let rows = studentListSorted.map((student, index) => {
        if (props.studentList[student].hasOwnProperty(props.activeClass)) {
            return <StudentRecordRow studentName={student} studentScore={props.studentList[student][props.activeClass]} clickEvent={props.clickEvent} key={index} updateStudentComparison={props.updateStudentComparison} studentComparison={props.studentComparison} />
        }
    });

    return (
        <div className="course-roster">
            <div className="course-roster--header">
                <div className="course-roster--row_student-name">Name</div>
                <div className="course-roster--row_score">Score</div>
                <div className="course-roster--row_student-compare">Compare</div>
            </div>
            {rows}
        </div>
    );
}

class StudentList extends Component{
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <div className="view-panel">
        <h1>{this.props.activeClass}: Student Roster</h1>
        <StudentTable activeClass={this.props.activeClass} studentList={this.props.studentList} studentComparison={this.props.studentComparison} clickEvent={this.props.changeStudent} updateStudentComparison={this.props.updateStudentComparison} />
      </div>
    );
  }
}

export default StudentList;