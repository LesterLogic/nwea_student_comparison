import React, { Component} from "react";
import StudentComparisonCheckbox from "./StudentComparisonCheckbox.js";

function RecordsTable(props) {
    let courses = Object.keys(props.recordList).sort();
    let rows = courses.map((subject, index) => {
        return (
            <tr key={index}>
                <td className="student-detail--table_name">{subject}</td>
                <td className="student-detail--table_score">{props.recordList[subject]}&#37;</td>
            </tr>
        );
    });

    return ( <table className="student-detail--table"><tbody>{rows}</tbody></table>);
}

class StudentComparison extends Component{
  constructor(props) {
    super(props);
    this.state = { students: props.studentList };
  }

  render() {
    return(
      <div className="view-panel">
        <h1>Student Comparison</h1>
        {this.props.studentComparison.length > 0 ? (
        <div className="compare-grid">
        {this.props.studentComparison.map((studentName, index) => 
            <div className="student-detail compare" key={index}>
                <div className="student-detail--header">
                    <div className="student-detail--header_name">{studentName}</div>
                    <div className="student-detail--header_compare"><StudentComparisonCheckbox studentComparison={this.props.studentComparison} studentName={studentName} updateStudentComparison={this.props.updateStudentComparison} /></div>
                </div>
                <RecordsTable recordList={this.props.studentList[studentName]} />
            </div>
        )}
        </div>
        ) : (
            <div>There are no students selected for comparison.</div>
        )}
      </div>
    );
  }
}

export default StudentComparison;