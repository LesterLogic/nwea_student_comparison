import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme";
import StudentDetail from "../components/StudentDetail.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("StudentDetail", () => {
    let props;
    let mountedStudentDetail;
    const AppStudentDetail = (props) => {
        if (!mountedStudentDetail) {
            mountedStudentDetail = mount(
                <StudentDetail {...props} />
            );
        }

        return mountedStudentDetail;
    }

    beforeEach(() => {
        props = {
            activeStudent: "Ward, Rose",
            studentRecord: defaultState.students["Ward, Rose"],
            studentComparison: ["Ward, Rose"],
            updateStudentComparison: function() { return; }
        };
        mountedStudentDetail = undefined;
    });

    it('should show correct student name', () => {
        expect(AppStudentDetail(props).find('h1').text()).toContain('Ward, Rose');
    });

    it('should show one row for each course', () => {
        expect(AppStudentDetail(props).find('tr').length).toEqual(4);
    });
});