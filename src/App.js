import React, { Component} from "react";
import Header from "./components/Header.js";
import ClassList from "./components/ClassList.js";
import StudentList from "./components/StudentList.js";
import StudentDetail from "./components/StudentDetail.js";
import StudentComparison from "./components/StudentComparison.js";

class App extends Component{
  constructor(props) {
    super(props);
    this.state = this.props.defaultState;

    this.handleActiveClassChange = this.handleActiveClassChange.bind(this);
    this.handleActiveStudentChange = this.handleActiveStudentChange.bind(this);
    this.handleActiveViewChange = this.handleActiveViewChange.bind(this);
    this.handleStudentComparisonChange =this.handleStudentComparisonChange.bind(this);
  }

  handleActiveClassChange(className) {
    this.setState({activeClass: className});
    this.handleActiveViewChange('StudentList');
  }

  handleActiveStudentChange(studentName) {
    this.setState({activeStudent: studentName});
    this.handleActiveViewChange('StudentDetail');
  }

  handleActiveViewChange(viewName) {
    this.setState({activeView: viewName});
  }

  handleStudentComparisonChange(studentList) {
    this.setState({studentComparison: studentList});
  }

  render() {
    let view =  <ClassList />;
    switch (this.state.activeView) {
      case 'StudentList':
        view = <StudentList studentList={this.state.students} activeClass={this.state.activeClass} changeStudent={this.handleActiveStudentChange} studentComparison={this.state.studentComparison} updateStudentComparison={this.handleStudentComparisonChange} />;
        break;
      case 'StudentDetail':
        view = <StudentDetail activeStudent={this.state.activeStudent} studentRecord={this.state.students[this.state.activeStudent]} studentComparison={this.state.studentComparison} updateStudentComparison={this.handleStudentComparisonChange} />;
        break;
      case 'StudentComparison':
        view = <StudentComparison studentList={this.state.students} studentComparison={this.state.studentComparison} updateStudentComparison={this.handleStudentComparisonChange} changeStudent={this.handleActiveStudentChange} />;
        break;
      default:
        view = <ClassList classList={this.state.classes} changeClass={this.handleActiveClassChange} />;
    }
    return(
      <div className="App">
        <Header changeView={this.handleActiveViewChange} activeClass={this.state.activeClass} activeStudent={this.state.activeStudent} studentComparison={this.state.studentComparison} />
        {view}
      </div>
    );
  }
}

export default App;
