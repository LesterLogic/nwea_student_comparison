import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme";
import StudentComparison from "../components/StudentComparison.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("StudentComparison", () => {
    let props;
    let mountedStudentComparison;
    const AppStudentComparison = (props) => {
        if (!mountedStudentComparison) {
            mountedStudentComparison = mount(
                <StudentComparison {...props} />
            );
        }

        return mountedStudentComparison;
    }     

    beforeEach(() => {
        props = {
            studentList: defaultState.students,
            studentComparison: ["Delong, Daniel", "Flaherty, Jenny", "Bordeton, Angela"],
            updateStudentComparison: function() { return; },
            changeStudent: function() { return; }
        };
        mountedStudentComparison = undefined;
    });

    it('should show the comparison grid when studentComparison is not empty', () => {
        expect(AppStudentComparison(props).find('.compare-grid').exists()).toEqual(true);
    });

    it('should show one record table for each student in studentComparison', () => {
        expect(AppStudentComparison(props).find('.student-detail--table').length).toEqual(3);
    });

    it('should show one record table for each student in studentComparison', () => {
        props = {
            studentList: defaultState.students,
            studentComparison: [],
            updateStudentComparison: function() { return; },
            changeStudent: function() { return; }
        };
        expect(AppStudentComparison(props).find('.compare-grid').exists()).toEqual(false);
    });

});