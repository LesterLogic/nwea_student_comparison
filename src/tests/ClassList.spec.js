import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme";
import ClassList from "../components/ClassList.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("ClassList", () => {
    let props;
    let mountedClassList;
    const AppClassList = (props) => {
        if (!mountedClassList) {
            mountedClassList = mount(
                <ClassList {...props} />
            );
        }

        return mountedClassList;
    }        

    beforeEach(() => {
        props = {
            clickEvent: function() { return; },
            classList: defaultState.classes
        };
        mountedClassList = undefined;
    });

    it('should show one link-button for each class in defaultState.classes', () => {
        expect(AppClassList(props).find('.link-button').length).toEqual(7);
    });

});