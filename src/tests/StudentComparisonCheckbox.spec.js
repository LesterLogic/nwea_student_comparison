import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme";
import StudentComparisonCheckbox from "../components/StudentComparisonCheckbox.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("StudentComparisonCheckbox", () => {
    let props;
    let mountedStudentComparisonCheckbox;
    const AppStudentComparisonCheckbox = (props) => {
        if (!mountedStudentComparisonCheckbox) {
            mountedStudentComparisonCheckbox = mount(
                <StudentComparisonCheckbox {...props} />
            );
        }

        return mountedStudentComparisonCheckbox;
    }        

    beforeEach(() => {
        props = {
            studentName: "Ward, Rose",
            studentComparison: ["Ward, Rose"]
        };
        mountedStudentComparisonCheckbox = undefined;
    });

    it('should show active state if student is in comparison list', () => {
        expect(AppStudentComparisonCheckbox(props).find('a').text()).toEqual('Remove');
    });

    it('should show inactive state if student is not in comparison list', () => {
        props.studentName = "Lester, Jon";
        expect(AppStudentComparisonCheckbox(props).find('a').text()).toEqual('Add');
    });

});