import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, configure } from "enzyme";
import StudentList from "../components/StudentList.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("StudentList", () => {
    let props;
    let mountedStudentList;
    const AppStudentList = (props) => {
        if (!mountedStudentList) {
            mountedStudentList = mount(
                <StudentList {...props} />
            );
        }

        return mountedStudentList;
    }

    beforeEach(() => {
        props = {
            studentList: defaultState.students,
            activeClass: "Art",
            changeStudent: function() { return; },
            studentComparison: [],
            updateStudentComparison: function() { return; }
        };
        mountedStudentList = undefined;
    });

    it('should show correct course name', () => {
        expect(AppStudentList(props).find('h1').text()).toContain('Art: Student Roster');
    });

    it('should show one row for each course', () => {
        expect(AppStudentList(props).find('.course-roster--header').length).toEqual(1);
        expect(AppStudentList(props).find('.course-roster--row').length).toEqual(5);
    });
});