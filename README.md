## Installation

1. Clone repo from git clone https://LesterLogic@bitbucket.org/LesterLogic/nwea_student_comparison.git
2. Install repo with `npm install`

---

## Starting the Application

1. To build the application run `npm run build`
2. To start the webserver run `npm run start`
3. Browse to the application at [http://localhost:8080](http://localhost:8080)

---

## Running Tests

1. To run tests run `jest` or `npm run test`
