import React, { Component} from "react";

class StudentComparisonCheckbox extends Component{
  constructor(props) {
    super(props);

    this.state = {studentComparison: this.props.studentComparison};
    
    this.handleCompareClick = this.handleCompareClick.bind(this);
  }

 handleCompareClick(e) {
      e.stopPropagation();
      let studentList = this.props.studentComparison;
      if (studentList.includes(this.props.studentName)) {
        studentList = studentList.filter((student) => student !== this.props.studentName);
        this.setState({studentComparison: studentList});
        this.props.updateStudentComparison(studentList);
      } else {
        studentList = studentList.concat(this.props.studentName).sort();
        this.setState({studentComparison: studentList});
        this.props.updateStudentComparison(studentList);
      }
  }

  render() {
    return (
        <span>
             <a href="#" onClick={this.handleCompareClick} className="comparisonToggle">{this.state.studentComparison.includes(this.props.studentName) ? "Remove" : "Add"}</a>
        </span>
    );
  }
}

export default StudentComparisonCheckbox;