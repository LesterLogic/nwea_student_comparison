import React from "react";

export const defaultState = {
    "activeView": "Home",
    "activeClass": "",
    "activeStudent": "",
    "studentComparison": [],
    "classes": ["Art", "Language Arts", "Math", "Music",  "Reading", "Science", "Social Studies"],
    "students": {
        "Jones, Alicia": {
            "Art": 91,
            "Language Arts": 100,
            "Math": 96,
            "Music": 80
        },
        "Harris, Colin": {
            "Language Arts": 67,
            "Math": 90,
            "Music": 62,
            "Reading": 97
        },
        "Flaherty, Jenny": {
            "Math": 86,
            "Music": 72,
            "Reading": 81,
            "Science": 71
        },
        "Delong, Daniel": {
            "Music": 60,
            "Reading": 100,
            "Science": 86,
            "Social Studies": 89
        },
        "Ward, Rose": {
            "Reading": 64,
            "Science": 87,
            "Social Studies": 93,
            "Art": 81
        },
        "Almond, Jason": {
            "Science": 77,
            "Social Studies": 79,
            "Art": 72,
            "Language Arts": 93
        },
        "Bordeton, Angela": {
            "Social Studies": 68,
            "Art": 92,
            "Language Arts": 73,
            "Math": 91
        },
        "Garnett, Barney": {
            "Art": 85,
            "Language Arts": 66,
            "Math": 94,
            "Music": 84
        },
        "Banister, Sandra": {
            "Language Arts": 75,
            "Math": 70,
            "Music": 76,
            "Reading": 93
        },
        "Bella, Eric": {
            "Math": 63,
            "Music": 66,
            "Reading": 62,
            "Science": 88
        }
    }
};
