import React, { Component} from "react";

function ChangeClassButton(props) {
    function handleClick(e) {
        e.preventDefault();
        props.clickEvent(props.subjectName);
    }

    return (
        <div onClick={handleClick} className="link-button">{props.subjectName}</div>
    );
};

const ButtonList = (props) => {
    let list = props.classList.map((subject, index) => <ChangeClassButton subjectName={subject} clickEvent={props.clickEvent} key={index} />);
    return ( <div>{list}</div>);
}

class ClassList extends Component{
  constructor(props) {
    super(props);
    this.state = { classList: props.classList };
  }

  render() {
    return(
      <div className="view-panel">
        <h1>Course Index</h1>
        <ButtonList classList={this.state.classList} clickEvent={this.props.changeClass} />
      </div>
    );
  }
}

export default ClassList;