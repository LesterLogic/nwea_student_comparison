import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, shallow, configure } from "enzyme";
import App from "../App.js";
import Header from "../components/Header.js";
import ClassList from "../components/ClassList.js";
import StudentList from "../components/StudentList.js";
import StudentDetail from "../components/StudentDetail.js";
import StudentComparison from "../components/StudentComparison.js";
import { defaultState } from "../data/data.js";

configure({adapter: new Adapter()});

describe("App", () => {
    let props;
    let mountedStudentApp;
    let StudentApp = (props) => {
        if (!mountedStudentApp) {
            mountedStudentApp = mount(
                <App {...props} />
            );
        }

        return mountedStudentApp;
    }        
    
    beforeEach(() => {
        props = {"defaultState": defaultState};
        mountedStudentApp = undefined;
    });

    it("expects defaultState to not be falsey", () => {
        expect(StudentApp(props).prop('defaultState')).not.toBeFalsy();
    });

    describe("View state changes", () => {
        beforeEach(() => {
            var props = {"defaultState": defaultState};
            var StudentApp = (props) => { shallow(<App {...props} />)};
        });

        it("renders a Header component", () => {
            expect(StudentApp(props).find(Header).length).toBe(1);
        });

        it("renders a ClassList by default", () => {
            expect(StudentApp(props).find(ClassList).length).toBe(1); 
        });

        it("renders a StudentList component when activeView is StudentList", () => {
            props.defaultState.activeView = 'StudentList';
            expect(StudentApp(props).find(StudentList).length).toBe(1);
        });

        it("renders a StudentDetail component when activeView is StudentDetail", () => {
            props.defaultState.activeView = 'StudentDetail';
            props.defaultState.activeStudent = 'Jones, Alicia';
            props.defaultState.activeClass = 'Art';
            expect(StudentApp(props).find(StudentDetail).length).toBe(1);
        });

        it("renders a StudentComparison component when activeView is StudentComparison", () => {
            props.defaultState.activeView = 'StudentComparison';
            expect(StudentApp(props).find(StudentComparison).length).toBe(1);
        });
    });
}); 