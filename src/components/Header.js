import React, { Component} from "react";

class Header extends Component{
  constructor(props) {
    super(props);

    this.handleHomeClick = this.handleHomeClick.bind(this);
    this.handleCourseClick = this.handleCourseClick.bind(this);
    this.handleStudentClick = this.handleStudentClick.bind(this);
    this.handleStudentComparisonClick = this.handleStudentComparisonClick.bind(this);
  }

  handleHomeClick(e) {
    e.preventDefault();
    this.props.changeView('Home');
  }

  handleCourseClick(e) {
    e.preventDefault();
    this.props.changeView('StudentList');
  }

  handleStudentClick(e) {
    e.preventDefault();
    this.props.changeView('StudentDetail');
  }

  handleStudentComparisonClick(e) {
    e.preventDefault();
    this.props.changeView('StudentComparison');
  }

  render() {
    return(
      <div id="global-header">
        <div className="title"><h1>NWEA: Student Comparison Application</h1></div>
        <nav>
          <div className="nav-item"><a href="#" onClick={this.handleHomeClick}>Course Index</a></div>
          {this.props.activeClass ? (<div className="nav-item"><a href="#" onClick={this.handleCourseClick}>Active Course: {this.props.activeClass}</a></div>) : ''}
          {this.props.activeStudent ? (<div className="nav-item {this.props.activeStudent ? '' : 'hidden'}"><a href="#" onClick={this.handleStudentClick}>Active Student: {this.props.activeStudent}</a></div>) : ''}
          {this.props.studentComparison.length > 0 ? (<div className="nav-item {this.props.studentComparison.length > 0 ? '' : 'hidden'}"><a href="#" onClick={this.handleStudentComparisonClick}>Student Comparison ({this.props.studentComparison.length})</a></div>) : ''}
        </nav>
      </div>
    );
  }
}

export default Header;