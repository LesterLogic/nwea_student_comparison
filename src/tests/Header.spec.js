import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import { mount, shallow, configure } from "enzyme";
import Header from "../components/Header.js";

configure({adapter: new Adapter()});

describe("Header", () => {
    let props;
    let mountedHeader;
    const AppHeader = (props) => {
        if (!mountedHeader) {
            mountedHeader = shallow(
                <Header {...props} />
            );
        }

        return mountedHeader;
    }        

    beforeEach(() => {
        props = {
            changeView: function() { return; },
            activeClass: 'Art',
            activeStudent: 'Jones, Alicia',
            studentComparison: ['Jones, Alicia']
        };
        mountedHeader = undefined;
    });

    it('should show the app title', () => {
        expect(AppHeader(props).find('.title').length).toEqual(1);
    });

    it('should show four nav-items when activeClass, activeStudent, and studentComparison set', () => {
        expect(AppHeader(props).find('.nav-item').length).toEqual(4);
    });

    it('should show three nav-items when activeClass, activeStudent are set', () => {
        props = {
            changeView: function() { return; },
            activeClass: 'Art',
            activeStudent: 'Jones, Alicia',
            studentComparison: []
        };
        expect(AppHeader(props).find('.nav-item').length).toEqual(3);
    });    
    
    it('should show two nav-items when activeClass is set', () => {
        props = {
            changeView: function() { return; },
            activeClass: 'Art',
            activeStudent: '',
            studentComparison: []
        };
        expect(AppHeader(props).find('.nav-item').length).toEqual(2);
    });   
    
    it('should show one nav-item when no active states are set', () => {
        props = {
            changeView: function() { return; },
            activeClass: '',
            activeStudent: '',
            studentComparison: []
        };
        expect(AppHeader(props).find('.nav-item').length).toEqual(1);
    });

});